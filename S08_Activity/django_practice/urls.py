from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('<groceryitem_name>/', views.groceryitem, name='viewgroceryitem'),
]